<?php
use \Bitrix\Main\Loader;

Loader::includeModule('iblock');

$iblockTypeId = CIBlockType::GetByID('forms');

if ($iblockTypeId) {
    CIBlockType::Delete('forms');
}