<?php
use \Bitrix\Main\Loader;

Loader::includeModule('iblock');

$cibt = new CIBlockType();
$cib = new CIBlock();
$cibp = new CIBlockProperty();

$iblockTypeId = $cibt->Add([
    'ID' => 'forms',
    'SECTIONS' => 'Y',
    'IN_RSS' => 'N',
    'SORT' => 100,
    'LANG' => [
        'ru' => [
            'NAME' => 'Формы',
            'SECTION_NAME' => 'Секции',
            'ELEMENT_NAME' => 'Элементы',
        ],
        'en' => [
            'NAME' => 'Forms',
            'SECTION_NAME' => 'Sections',
            'ELEMENT_NAME' => 'Elements',
        ],
    ],
]);

if (!$iblockTypeId) {
    echo 'Ошибка при создании типа инфоблока' . PHP_EOL;
    die;
}

$iblockId = $cib->Add([
    'ACTIVE' => 'Y',
    'NAME' => 'Форма обратной связи',
    'CODE' => 'feedback_form',
    'LIST_PAGE_URL' => '',
    'DETAIL_PAGE_URL' => '',
    'SECTION_PAGE_URL' => '',
    'IBLOCK_TYPE_ID' => 'forms',
    'LID' => ['s1'],
    'SORT' => 500,
    'GROUP_ID' => ['2' => 'R'],
    'VERSION' => 2,
    'BIZPROC' => 'N',
    'WORKFLOW' => 'N',
    'INDEX_ELEMENT' => 'N',
    'INDEX_SECTION' => 'N',
]);

if (!$iblockId) {
    echo 'Ошибка при создании инфоблока' . PHP_EOL;
    die;
} else {
    echo 'Создан инфоблок с ID ' . $iblockId . PHP_EOL;
}

$props = [
    'name' => 'Имя',
    'topic' => 'Тема обращения',
    'text' => 'Текст обращения',
];

foreach ($props as $code => $name) {
    $propId = $cibp->Add([
        'NAME' => $name,
        'ACTIVE' => 'Y',
        'SORT' => '500',
        'CODE' => $code,
        'PROPERTY_TYPE' => 'S',
        'USER_TYPE' => '',
        'MULTIPLE' => 'N',
        'IS_REQUIRED' => 'Y',
        'FILTRABLE' => 'Y',
        'IBLOCK_ID' => $iblockId,
    ]);

    if (!$propId) {
        echo 'Ошибка при создании параметра инфоблока: ' . $name . PHP_EOL;
        echo $cibp->LAST_ERROR;
        die;
    }
}