<?php
use \Bitrix\Main\Loader;

Loader::includeModule('main');

$typeId = CEventType::Add([
    'LID' => 'ru',
    'EVENT_NAME' => 'FEEDBACK_FORM_ADD',
    'EVENT_TYPE' => 'email',
    'NAME' => 'Новое обращение через форму обратной связи',
    'DESCRIPTION' => '
    #NAME# - Имя
    #TOPIC# - Тема обращения
    #TEXT# - Текст обращения
    ',
]);

if (!$typeId) {
    echo 'Ошибка при создании типа почтового события' . PHP_EOL;
    die;
}