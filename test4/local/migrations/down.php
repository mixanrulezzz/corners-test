<?php

include_once 'setup/migration_prolog.php';

include_once 'setup/down/delete_feedback_iblock.php';
include_once 'setup/down/delete_feedback_event.php';

include_once 'setup/migration_epilog.php';