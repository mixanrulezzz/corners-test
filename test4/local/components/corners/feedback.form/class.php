<?php


use Bitrix\Iblock\IblockTable;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;

class FeedbackForm extends \CBitrixComponent {

    /**
     * Сохранить результат из формы
     * @return void
     */
    protected function saveFormResult(): void
    {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();

        $request = Application::getInstance()->getContext()->getRequest();

        header('Content-type: application/json');

        try {
            if (!check_bitrix_sessid()) {
                throw new Exception('Ошибка сервера');
            }

            $postFields = $request->getPostList()->toArray();

            $this->addResultToFeedbackIblock($postFields);
            CEvent::Send('FEEDBACK_FORM_ADD', 'ru', [
                'NAME' => $postFields['name'],
                'TOPIC' => $postFields['topic'],
                'TEXT' => $postFields['text'],
            ]);

            echo json_encode(['res' => true]);
        } catch (Exception $e) {
            echo json_encode(['res' => false, 'message' => $e->getMessage()]);
        }

        die();
    }

    /**
     * Добавить новый элемент с обращением в инфоблок для обратной связи
     * @param array $fields
     * @return int|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function addResultToFeedbackIblock(array $fields): ?int
    {
        $iblockId = IblockTable::getRow(['filter' => ['=CODE' => 'feedback_form'], 'select' => ['ID']])['ID'];
        $props = array_column(
            PropertyTable::getList(['filter' => ['=IBLOCK_ID' => $iblockId], 'select' => ['ID', 'CODE']])->fetchAll(),
            'ID',
            'CODE'
        );

        $iBlockElem = new CIBlockElement();
        $elemId = $iBlockElem->Add([
            'IBLOCK_ID' => $iblockId,
            'NAME' => 'Обращение за ' . (new DateTime())->format('d.m.Y H:i:s'),
            'PROPERTY_VALUES' => [
                $props['name'] => $fields['name'],
                $props['topic'] => $fields['topic'],
                $props['text'] => $fields['text'],
            ],
        ]);

        if (!$elemId) {
            throw new Exception($iBlockElem->LAST_ERROR);
        }

        return $elemId;
    }

    public function executeComponent()
    {
        Loader::includeModule('main');
        Loader::includeModule('iblock');

        $request = Application::getInstance()->getContext()->getRequest();

        if ($request->getPost('corners_feedback_form') == "Y") {
            $this->saveFormResult();
        }

        $this->includeComponentTemplate();
    }
}