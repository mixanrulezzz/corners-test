document.addEventListener('DOMContentLoaded', () => {
    const forms = document.getElementsByName('corners_feedback_form');

    for (let i = 0; i < forms.length; i++) {
        forms[i].removeEventListener('submit', onFormSubmit);
        forms[i].addEventListener('submit', onFormSubmit);
    }

    function onFormSubmit(e) {
        e.preventDefault();

        const formData = new FormData(e.srcElement);

        fetch(window.location.href, {
            mode: 'cors',
            method: 'POST',
            body: formData,
        }).then(response => {
            if (!response.ok) {
                throw new Error(`Error! status: ${response.status}`);
            }

            return response.json();
        }).then(result => {
            if (result.res) {
                e.srcElement.parentElement.innerHTML = '<p>Ваше сообщение успешно зарегистрировано.</p>';
            } else {
                e.srcElement.parentElement.innerHTML = '<p>Произошла ошибка: ' + result.message + '</p>';
            }
        })
        .catch(error => { console.log('request failed', error); });
    }
});

