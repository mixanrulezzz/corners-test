<?php
/** @var array $arResult */
?>

<div class="form-container">
    <form name="corners_feedback_form">
        <h3>Форма обратной связи</h3>
        <?= bitrix_sessid_post() ?>
        <input type="hidden" name="corners_feedback_form" value="Y">

        <div class="form-input-container">
            <label for="name">Имя</label>
            <input id="name" name="name" type="text" required>
        </div>

        <div class="form-input-container">
            <label for="topic">Тема обращения</label>
            <input id="topic" name="topic" type="text" required>
        </div>

        <div class="form-input-container">
            <label for="text">Текст обращения</label>
            <textarea id="text" name="text" required></textarea>
        </div>

        <div>
            <button type="submit">Отправить</button>
        </div>
    </form>
</div>
