<?php


/**
 * Класс для расчета цены с наценкой
 */
class PriceMarkup
{
    public function __construct(
        protected float $originalPrice
    )
    {
    }

    /**
     * Получить цену с наценкой
     * @return float
     */
    public function getPriceWithMarkup(): float
    {
        return $this->originalPrice + $this->loginMarkup();
    }

    /**
     * Наценка за согласные буквы в логине
     * @return float
     */
    protected function loginMarkup(): float
    {
        global $USER;

        if (!$USER->IsAuthorized()) {
            return 0.0;
        }

        $onlyConsonant = preg_replace('/[^bcdfghjklmnpqrstvwxyz]/ui', '', $USER->GetLogin());

        return $this->originalPrice * strlen($onlyConsonant) / 100;
    }
}